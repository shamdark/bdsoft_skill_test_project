<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @property  session
 */
class Division extends CI_Model{

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

        /**
     * Action for adding an Division
     *
     * @uses    To add an Division
     * @access  public
     * @param   array $data
     * @return  boolean
     * @author  Ahatasham
     */
    public function add($data) {
        return $this->db->insert('division', $data);
    }


     /**
     * 
     * @author: Ahatasham
     */


    public function get_list($offset,$limit) {         
        $this->db->select('*');
        $this->db->from('division');
        $this->db->limit($limit, $offset); 
        $this->db->order_by('division.id', 'DESC'); 
        $query = $this->db->get();          
        return $query->result_array();
    }
    /**
     * Count number of rows
     * @author  :   Abdul Matin
     * @uses    :   To count row
     * @access  :   public
     * @return  :   int
     */

    /**
     * @updated Anis Alamgir
     * @date 18-Jan-2010
     * Add new param $cond=""
     */
    function row_count($cond = '') {

        return $this->db->count_all_results('division');
    }

    public function get_division_data($id=null){

        if(empty($id)){
            $query = $this->db->query("SELECT * FROM division ORDER BY id DESC")->result();
        }
        else{
            $query = $this->db->query("SELECT * FROM division WHERE id = $id")->result();
        }
        
        return $query;
    }


    public function edit_division($data){
        $this->db->where('id', $data['id']);
        return $this->db->update('division', $data);
    }

    public function delete_division($id){

        $this->db->where('id',$id);
        return $this->db->delete('division');

    }

    public function edit_data($id,$fields){
          $this ->db->where('id',$id)->update('division',$fields);
    }



    
    
       
}