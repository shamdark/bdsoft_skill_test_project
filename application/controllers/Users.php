<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller{

    function __construct() {
        parent::__construct();
        if ($this->session->userdata('user_id') == '' && $this->session->userdata('user_name') == '') {
            redirect('Logins');
        }
        $this->load->library('pagination');
        $this->load->model(array('Division','District','User'), '', TRUE);
       
    }
   
    /**
    * Action for default Division list view page
    * 
    * @uses    To view default Division list page
    * @access   public
    * @param   void
    * @return  void
    * @author  Ahatasham
    */
   

    public function index(){

        $data = array();
        $data['user_role'] = $this->session->userdata();
        $data['heading'] = "Manage User";
        $data['title'] = "User List";
        $this->load->view('Users/index',$data);
     }

    public function get_pagination_data($offset=0) {
        $per_page = 5;
        if($offset != 0){
            $offset = ($offset-1) * $per_page;
        }       
        $total_rows = $this->User->row_count();
        $data_list = $this->User->get_list($offset,$per_page);
        $config['base_url'] = base_url().'index.php/Users/get_pagination_data';
        $config['use_page_numbers'] = TRUE;
        $config['next_link'] = '>>';
        $config['prev_link'] = '<<';
        $config['total_rows'] = $total_rows;
        $config['per_page'] = $per_page;
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $data['record_list'] = $data_list;
        echo json_encode($data);        
     }
    


    public function add(){

        $this->_prepare_validation();

           if($_POST){  
                $data=$this->_get_posted_data();
                $data['status']=1;
                // echo "<pre>";print_r($data);die;
                if ($this->form_validation->run() === TRUE){
                    
                    if($this->User->add($data)){
                        $this->session->set_flashdata('message',ADD_MESSAGE);
                        
                    }
                    else{
                        $this->session->set_flashdata('warning',ADD_MESSAGE);   
                    }

                    redirect('/Users/index');
                }
                else{
                   redirect('Users');
                }  
            } 
        }


    public function edit(){
        $id = $this->input->post('id');


           $callback_message = array();
           $district_details = $this->District->get_district_data($id);

           foreach ($district_details as $deatils) {
                $callback_message['district_name'] = $deatils->district_name;
                $callback_message['division_id'] = $deatils->division_id;
                $callback_message['id'] = $deatils->id;

              }
           $callback_message['get_all_division'] = $this->Division->get_division_data();

          echo json_encode($callback_message); 
         

      }
       
    public function update(){

      if(isset($_POST)){
            $data = $this->_get_posted_data();
            $data['id'] = $this->input->post("id");

            if($this->District->edit_district($data)){
               $this->session->set_flashdata('message',EDIT_MESSAGE); 
             }

            else{
             $this->session->set_flashdata('message',WARNING_MESSAGE); 
            }
           
          redirect('Districts/index/', 'refresh');
      }
    }


    public function delete_user($id=null){
        $callback_message = array();
        $id = $this->input->post('id');
        if(empty($id) || $id == ""){

         $this->session->set_flashdata('warning',WARNING_MESSAGE); 
         redirect('Users/index', 'refresh');
        }

       
        if($this->User->delete_user($id)){

            // $this->session->set_flashdata('message',DELETE_MESSAGE);
            $callback_message['status']="success";
        }else{

               $this->session->set_flashdata('warning',WARNING_MESSAGE);
               $callback_message['status']="failed";
            }

         echo json_encode($callback_message);
    }
    

        /**
     * Action for maping the form data to database fields
     * 
     * @uses    To map the form data to database fields
     * @access  public
     * @param   void 
     * @return  array
     * @author  Ahatasham
     */ 
    function _get_posted_data(){
        $data=array();
        
        $data['name']=$this->input->post('name');
        $data['designation']=$this->input->post('designation');
        $data['user_level']=$this->input->post('admin_level');
        $data['division_id']=$this->input->post('division_id');
        $data['district_id']=$this->input->post('district_id');
        $data['password']=$this->input->post('password');
              
        return $data;       
    }


        /**
     * Action for setting validation rules
     * 
     * @uses    To set validation rules
     * @access  private
     * @param   void 
     * @return  void
     * @author  Tuhin
     */ 
    function _prepare_validation(){
        //Loading Validation Library to Perform Validation numeric
        
        $this->load->library('form_validation');    
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        //Setting Validation Rule
        $this->form_validation->set_rules('district_id','district_name','trim|xss_clean|required');  
        $this->form_validation->set_rules('division_id','division_name','trim|xss_clean|required');  
        $this->form_validation->set_rules('admin_level','admin_level','trim|xss_clean|required');  
        $this->form_validation->set_rules('designation','designation','trim|xss_clean|required');  
        $this->form_validation->set_rules('name','name','trim|xss_clean|required');  
        $this->form_validation->set_rules('password','password','trim|xss_clean|required');  
         
    }


    /**
     * Prepares data for combo
     * 
     * @uses    To load data
     * @access  public
     * @param   void 
     * @return  array
     * @author  Ahatasham
     */ 
    
    public function _load_combo_data(){

        //This function is for listing of designations  
        $data['division_info'] = $this->District->division_info();     
        return $data;
    }

    public function get_data_for_add_user(){

        $callback_message['get_division_data'] = $this->Division->get_division_data();
        $callback_message['get_district_data'] = $this->District->get_district_data();
        $callback_message['get_role_data'] = $this->User->get_role_data();
        
        echo json_encode($callback_message);
    }





     public function editdata(){

        $callback_message = array();
        If( $_SERVER['REQUEST_METHOD']  != 'POST'  ){
            redirect('table');
        }
        
        $id = $this->input->post('id',true);
        $title = $this->input->post('title',true); 
        $field_name = $this->input->post('field_name',true);       
        $fields = array($field_name => $title);        
        $this->User->edit_data($id,$fields);
        $callback_message['status'] = "success"; 
        $callback_message['role'] = $this->User->get_role_by_user_id($title);
          

        echo json_encode($callback_message);
        
      
          
    }


    public function role_wise_user_access_check(){

        $callback_message = array();
        $callback_message['permission'] = 0;

        $logged_user_info = $this->session->userdata();
        $user_id = $this->input->post('id');
        $division_id = $this->input->post('division_id');
        $district_id = $this->input->post('district_id');

        if($logged_user_info['is_admin'] == 1){
            $callback_message['permission'] = 1;
        }else{

            if($logged_user_info['is_admin'] != 1 && $logged_user_info['is_divisional_admin'] == 1){
                if($logged_user_info['division'] == $division_id){
                   $callback_message['permission'] = 1; 
                }
            }else{
                if($logged_user_info['district'] == $district_id){
                   $callback_message['permission'] = 1;  
                }
            }
        }

        echo json_encode($callback_message);

    }



    public function get_district_id_by_division_id() {
        $this->output->enable_profiler(FALSE);
        $callback_message = array();
        $division_id = $this->input->post('selected_division_id');
        $callback_message['status'] = 'failure';
        if (empty($division_id)) {
            $callback_message['message'] = 'Select a division';
        } else {
            $district_info = $this->User->get_district_list($division_id);
            
            foreach ($district_info as $row) {
                $callback_message['status'] = 'success';
                $callback_message['district_id'][] = $row['district_id'];
                $callback_message['district_name'][] = $row['district_name'];
            }
        }
        echo json_encode($callback_message);

    }
}