<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Divisions extends CI_Controller{

    function __construct() {
        parent::__construct();
        if ($this->session->userdata('user_id') == '' && $this->session->userdata('user_name') == '') {
            redirect('Logins');
        }
        $this->load->library('pagination');
        $this->load->model(array('Division'), '', TRUE);
       
    }
   
    /**
    * Action for default Division list view page
    * 
    * @uses    To view default Division list page
    * @access   public
    * @param   void
    * @return  void
    * @author  Ahatasham
    */
   

  public function index(){
        $data = array();
        $data['user_role'] = $this->session->userdata();
        $data['heading'] = "Manage Division";
        $data['title'] = "Division List";
        $this->load->view('Divisions/index',$data);
    }

 public function get_pagination_data($offset=0) {
        $per_page = 5;
        if($offset != 0){
            $offset = ($offset-1) * $per_page;
        }       
        $total_rows = $this->Division->row_count();
        $data_list = $this->Division->get_list($offset,$per_page);
        $config['base_url'] = base_url().'index.php/Divisions/get_pagination_data';
        $config['use_page_numbers'] = TRUE;
        $config['next_link'] = '>>';
        $config['prev_link'] = '<<';
        $config['total_rows'] = $total_rows;
        $config['per_page'] = $per_page;
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $data['record_list'] = $data_list;
        echo json_encode($data);        
    }

    public function add(){

        $this->_prepare_validation();

           if($_POST){  
                $data=$this->_get_posted_data();
                // echo "<pre>";print_r($data);die;
                if ($this->form_validation->run() === TRUE){
                    
                    if($this->Division->add($data)){
                        $this->session->set_flashdata('message',ADD_MESSAGE);
                        
                    }
                    else{
                        $this->session->set_flashdata('message',ADD_MESSAGE);   
                    }

                    redirect('/Divisions/index');
                }
                else{
                   redirect('Divisions');
                }  
            } 
        }


    public function edit(){
        $id = $this->input->post('id');


           $callback_message = array();
           $division_details = $this->Division->get_division_data($id);
           foreach ($division_details as $deatils) {
                $callback_message['division_name'] = $deatils->division_name;
                $callback_message['id'] = $deatils->id;

              }

            echo json_encode($callback_message); 
         

      }


    public function update(){

      if(isset($_POST)){
            $data = $this->_get_posted_data();
            $data['id'] = $this->input->post("id");

            if($this->Division->edit_division($data)){
               $this->session->set_flashdata('message',EDIT_MESSAGE); 
             }

            else{
             $this->session->set_flashdata('message',WARNING_MESSAGE); 
            }
           
          redirect('Divisions/index/', 'refresh');
      }
    }


    public function delete_division(){

        $callback_message = array();
        $id = $this->input->post('id');
        if(empty($id) || $id == ""){

         $this->session->set_flashdata('warning',WARNING_MESSAGE); 
         redirect('Divisions/index', 'refresh');
        }

       
        if($this->Division->delete_division($id)){

            // $this->session->set_flashdata('message',DELETE_MESSAGE);
            $callback_message['status']="success";
             
           }else{

               $this->session->set_flashdata('warning',WARNING_MESSAGE);
               $callback_message['status']="failed";
            }

         echo json_encode($callback_message); 
    }
    

        /**
     * Action for maping the form data to database fields
     * 
     * @uses    To map the form data to database fields
     * @access  public
     * @param   void 
     * @return  array
     * @author  Ahatasham
     */ 
    function _get_posted_data(){
        $data=array();
        $data['division_name']=$this->input->post('division_name');
              
        return $data;       
    }


        /**
     * Action for setting validation rules
     * 
     * @uses    To set validation rules
     * @access  private
     * @param   void 
     * @return  void
     * @author  Tuhin
     */ 
    function _prepare_validation(){
        //Loading Validation Library to Perform Validation numeric
        
        $this->load->library('form_validation');    
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        //Setting Validation Rule
        $this->form_validation->set_rules('division_name','Division_name','trim|xss_clean|required');  
         
    }

  

     public function editdata(){
        If( $_SERVER['REQUEST_METHOD']  != 'POST'  ){
            redirect('table');
        }
        
        $id = $this->input->post('id',true);
        $title = $this->input->post('title',true); 
        $field_name = $this->input->post('field_name',true);       
        $fields = array($field_name => $title);        
        $this->Division->edit_data($id,$fields);
        
        echo "Successfully saved";
          
    }


    public function role_wise_user_access_check(){

        $callback_message = array();
        $callback_message['permission'] = 0;

        $logged_user_info = $this->session->userdata();
        $division_id = $this->input->post('id');

        if($logged_user_info['is_admin'] == 1){
            $callback_message['permission'] = 1;
        }else{

            if($logged_user_info['is_admin'] != 1 && $logged_user_info['is_divisional_admin'] == 1){
                if($logged_user_info['division'] == $division_id){
                   $callback_message['permission'] = 1; 
                }
            }else{
                
                   $callback_message['permission'] = 0;  
               
            }
        }

        echo json_encode($callback_message);

    }
}